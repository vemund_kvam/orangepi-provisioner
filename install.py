import time
import getpass
def unpack_image_osx():
    print('Unpacking image')
    pass

def instruct_insert_sd_card():
    print('Insert SD card')
    time.sleep(2)
    pass

def locate_sd_card():
    print('Locating sd card...')
    time.sleep(2)
    pass

def upload_to_sd_card():
    print('uploading to sd card')
    time.sleep(2)
    pass

def unmount_sd_card():
    print('Unmounting sd card')
    time.sleep(2)

def instruct_move_sd_card():
    print('Move sd card to orangepi and boot it')
    time.sleep(2)
    pass

def locate_orange_pi():
    # nmap
    print('Locating orangepi')
    time.sleep(2)
    pass

def change_password():
    password=getpass.getpass('Set orangepi password:')
    if len(password)<=2:
        print("Choose a password longer than 2 characters")
        change_password()
    elif password != getpass.getpass('Verify password:'):
        print("Passwords did not match")
        change_password()
    else:
        print(password)

change_password()
unpack_image_osx()
instruct_insert_sd_card()
locate_sd_card()
upload_to_sd_card()
unmount_sd_card()
instruct_move_sd_card()
locate_orange_pi()
change_password()

